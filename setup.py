from setuptools import setup, find_packages

setup(
    name='quant_torch_deep_learning',
    version='0.1.0',
    packages=find_packages(),
    description = 'Quantitative analytics package based on PyTorch for deep learning of derivative pricing',
    url = 'https://bitbucket.org/quant-analytics-torch/quant_torch_deep_learning/',
    author_email = 'quant.analytics.torch@gmail.com',
    license = 'MIT License',
    zip_safe=False
)